from django.db import models
from django.db.models import Model, CASCADE


class Store(Model):

    name = models.TextField()
    city = models.TextField()

    class Meta:
        db_table = 'store'
