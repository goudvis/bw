from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^new-booze-watch$', views.WatchView.as_view(), name='watch')
]
