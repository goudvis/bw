from django.http import HttpRequest
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View
from django.db import connection
from datetime import date, timedelta
from bw.models import Store


class WatchView(View):

    def get(self, request, *args, **kwargs):

        def selected_option(id, selected_stores):
            return 'selected="selected"' if str(id) in selected_stores else ''

        selected_stores = request.session.get('store', [])
        stores = [f'<option value="{store.id}" {selected_option(store.id, selected_stores)}>{store.city} &gt; {store.name}</option>' for store in Store.objects.all().order_by('city', 'name')]

        new_inv = []
        if len(selected_stores)>0 :
           with connection.cursor() as cursor:
                cursor.execute("""
select 
    product.id, primary_category, secondary_category, image_thumb_url, name, origin, price_in_cents, alcohol_content, store_id, added_on
from inventory 
    join product on inventory.product_id = product.id 
where 
    store_id in %s
    and added_on>%s
group by
    product_id
order by
    primary_category, secondary_category""", [selected_stores, date.today() - timedelta(days=3)])
                desc = cursor.description
                new_inv = [dict(zip([col[0] for col in desc], row)) for row in cursor.fetchall()]

        def price(price_in_cents):
            return "${:,.2f}".format(price_in_cents/100)

        primary_categories = {}
        for row in new_inv:
            row['price'] = price(row['price_in_cents'])
            primary_categories[row['primary_category']] = True

        context = {
            "stores": ''.join(stores),
            "new_inv": new_inv,
            "primary_categories": primary_categories.keys()
        }
        return render(request, 'watch.html', context=context)

    def post(self, request: HttpRequest):
        request.session['store'] = request.POST.getlist('store')
        return redirect(reverse('watch'))
