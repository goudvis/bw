from django.shortcuts import render
from django.views import View

from bw.models import Store


class HomeView(View):

    def get(self, request, *args, **kwargs):
        stores = [f'<option value="{store.id}">{store.city} &gt; {store.name}</option>' for store in Store.objects.all().order_by('city', 'name')]
        context = {
            "stores": ''.join(stores)
        }
        return render(request, 'home.html', context=context)
