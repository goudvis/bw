from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(|index.html)$', views.HomeView.as_view(), name='index')
]
