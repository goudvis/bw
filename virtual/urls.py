from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^virtual-store$', views.VirtualView.as_view(), name='virtual')
]
