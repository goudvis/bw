from django.http import HttpRequest
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View
from django.db import connection
from datetime import date, timedelta
from bw.models import Store


class VirtualView(View):

    def ajax(self, request):
        if 'nav' in request.GET:
            return render(request, 'nav.html')

    def get(self, request: HttpRequest, *args, **kwargs):
        if request.is_ajax():
            return self.ajax(request)

        def selected_option(id, selected_stores):
            return 'selected="selected"' if str(id) in selected_stores else ''

        selected_stores = request.session.get('store', [])
        stores = [f'<option value="{store.id}" {selected_option(store.id, selected_stores)}>{store.city} &gt; {store.name}</option>' for store in Store.objects.all().order_by('city', 'name')]

        context = {
            'stores': stores
        }
        return render(request, 'virtual.html', context)

    def post(self, request: HttpRequest):
        request.session['store'] = request.POST.getlist('store')
        return redirect(reverse('virtual'))
